import 'package:pustak/provider/categories.dart';

List<Categories> _categories = [
  Categories(
    'c1',
    'School',
    'https://cdn4.iconfinder.com/data/icons/flatified/512/photos.png',
  ),
  Categories(
    'c2',
    'Book',
    'https://cdn4.iconfinder.com/data/icons/flatified/512/photos.png',
  ),
  Categories(
    'c3',
    'Bag',
    'https://cdn4.iconfinder.com/data/icons/flatified/512/photos.png',
  ),
  Categories(
    'c4',
    'Stationery',
    'https://cdn4.iconfinder.com/data/icons/flatified/512/photos.png',
  ),
  Categories(
    'c5',
    'Stationery',
    'https://cdn4.iconfinder.com/data/icons/flatified/512/photos.png',
  ),
  Categories(
    'c6',
    'Stationery',
    'https://cdn4.iconfinder.com/data/icons/flatified/512/photos.png',
  ),
  // Categories(
  //   'c7',
  //   'Stationery',
  //   'https://cdn4.iconfinder.com/data/icons/flatified/512/photos.png',
  // ),
  // Categories(
  //   'c8',
  //   'Stationery',
  //   'https://cdn4.iconfinder.com/data/icons/flatified/512/photos.png',
  // ),
  // Categories(
  //   'c9',
  //   'Stationery',
  //   'https://cdn4.iconfinder.com/data/icons/flatified/512/photos.png',
  // ),
];

List<Categories> get items {
  return [..._categories];
}
