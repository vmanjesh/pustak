import 'package:flutter/material.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final String imageUrl;
  final int price;
  final int priceOff;
  final bool isAvailable;

  Product({
    this.id,
    this.title,
    this.description,
    this.imageUrl,
    this.price,
    this.priceOff,
    this.isAvailable,
  });
}
