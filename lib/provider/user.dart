import 'package:flutter/material.dart';

class User with ChangeNotifier {
  final String id;
  final String firstName;
  final String lastName;
  final String emailId;
  final String password;
  final int phoneNumber;
  final String address1;
  final String address2;
  final String city;
  final String state;
  final String country;
  final int zipCode;

  User({
    @required this.id,
    @required this.firstName,
    @required this.lastName,
    @required this.emailId,
    @required this.password,
    @required this.phoneNumber,
    @required this.address1,
    @required this.address2,
    @required this.city,
    @required this.state,
    @required this.country,
    @required this.zipCode,
  });
}
