import 'package:flutter/material.dart';

class Categories with ChangeNotifier {
  final String id;
  final String title;
  final String imageUrl;

  Categories(this.id, this.title, this.imageUrl);
}
