import 'package:flutter/material.dart';
import 'package:pustak/screens/cart_screen.dart';
import 'package:pustak/screens/category_screen.dart';
import 'package:pustak/screens/main_page.dart';
import 'package:pustak/screens/profile_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int id = 0;
  @override
  Widget build(BuildContext context) {
    final _tab = [
      MainPage(),
      CategoryScreen(),
      CartScreen(),
      ProfileScreen(),
    ];
    void _onTapped(int index) {
      setState(() {
        id = index;
      });
    }

    return Scaffold(
      body: _tab[id],
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.store), label: 'Home'),
          BottomNavigationBarItem(
              icon: Icon(Icons.view_list), label: 'Category'),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart), label: 'Cart'),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: 'Profile'),
        ],
        type: BottomNavigationBarType.shifting,
        currentIndex: id,
        selectedItemColor: Colors.green[400],
        unselectedItemColor: Colors.grey,
        onTap: _onTapped,
        elevation: 5,
        iconSize: 40,
      ),
    );
  }
}
