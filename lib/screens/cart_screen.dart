import 'package:flutter/material.dart';
import 'package:pustak/provider/categories.dart';
import 'package:pustak/widgets/bottom_button.dart';
import 'package:pustak/widgets/cart_item.dart';

class CartScreen extends StatelessWidget {
  List<Categories> _list;
  int id = 1;
  @override
  Widget build(BuildContext context) {
    // final products = Provider.of<Categories>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Text('Cart'),
        actions: [
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.search,
                  size: 26.0,
                ),
              )),
        ],
      ),
      backgroundColor: Colors.indigo[50],
      body: SafeArea(
        child: ListView.builder(
          itemCount: 2,
          itemBuilder: (BuildContext context, int index) {
            return CartItem();
          },
        ),
      ),
      persistentFooterButtons: [
        BottomCartButton(),
      ],
    );
  }
}
