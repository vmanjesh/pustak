import 'package:flutter/material.dart';
import 'package:pustak/provider/categories.dart';
import 'package:pustak/provider/dummy_data.dart';
import 'package:pustak/widgets/category_bottom_sheet.dart';

class CategoryScreen extends StatelessWidget {
  List<Categories> _list = items;
  @override
  Widget build(BuildContext context) {
    Widget _cateList = Container(
      height: MediaQuery.of(context).size.height,
      child: ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: () {
              _settingModalBottomSheet(context);

              // Navigator.of(context).push(
              //   MaterialPageRoute(
              //     builder: (context) => CategoryBottom(),
              //   ),
              // );
            },
            child: Container(
              margin: EdgeInsets.only(bottom: 15),
              padding: const EdgeInsets.all(9.0),
              height: 75,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '  ${_list[index].title}',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 25,
                    ),
                  ),
                  Icon(Icons.chevron_right),
                ],
              ),
            ),
          );
        },
      ),
    );
    return Scaffold(
      appBar: AppBar(
        title: Text('Category'),
        actions: [
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.search,
                  size: 26.0,
                ),
              )),
        ],
      ),
      backgroundColor: Colors.indigo[50],
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(8),
          child: _cateList,
        ),
      ),
    );
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: ListView.builder(
              itemCount: 25,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(title: Text('Item $index'));
              },
            ),
          );
        });
  }
}
