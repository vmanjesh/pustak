import 'package:flutter/material.dart';

import 'package:pustak/provider/categories.dart';
import 'package:pustak/provider/dummy_data.dart';
import 'package:pustak/widgets/carousel_item.dart';
import 'package:pustak/widgets/category_item.dart';
import 'package:pustak/widgets/featured_item.dart';
import 'package:pustak/widgets/main_footer.dart';
import 'package:pustak/widgets/recommended_item.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  ScrollController scrollController;
  final double expandedHight = 250.0;

  @override
  void initState() {
    super.initState();
    scrollController = new ScrollController();
    scrollController.addListener(() => setState(() {}));
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  List<Categories> _list = items;

  @override
  Widget build(BuildContext context) {
    Widget _categoriesItem = Container(
      // height: 300,
      child: GridView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        itemCount: _list.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            height: 150,
            child: Card(
              child: Stack(children: [
                Container(
                  child: Image.network(_list[index].imageUrl),
                  height: 100,
                  width: 100,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Text(_list[index].title)),
                ),
              ]),
            ),
          );
        },
      ),
    );

    return Scaffold(
      backgroundColor: Colors.indigo[50],
      appBar: AppBar(
        title: Container(
          width: 200,
          child: Image.asset(
            'assets/pustaklab.png',
          ),
        ),
        actions: [
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {},
                child: Icon(
                  Icons.search,
                  size: 26.0,
                ),
              )),
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(5.0),
                child: CarouselItem(),
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 8.0,
                      top: 8,
                    ),
                    child: Text(
                      'Categories',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Container(
                  height: 80,
                  child: CategoryList(),
                ),
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 8.0,
                      // top: 8,
                    ),
                    child: Text(
                      'Featured Product',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Container(
                  height: 400,
                  child: FeatureItem(),
                ),
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 8.0,
                      // top: 8,
                    ),
                    child: Text(
                      'Recommended ',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                  ),
                ],
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Container(
                  height: 580,
                  child: RecommendedItem(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  height: 40,
                  child: MainFooter(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
