import 'package:flutter/material.dart';
import 'package:pustak/provider/categories.dart';
import 'package:pustak/provider/dummy_data.dart';

class CategoryList extends StatelessWidget {
  List<Categories> _list = items;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        scrollDirection: Axis.vertical,
        itemCount: _list.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
            margin: EdgeInsets.only(bottom: 15),
            padding: const EdgeInsets.all(9.0),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text('_list[index].title'),
                ),
                Icon(Icons.chevron_right),
              ],
            ),
          );
        });
  }
}
