import 'package:flutter/material.dart';

class CartItem extends StatefulWidget {
  @override
  _CartItemState createState() => _CartItemState();
}

class _CartItemState extends State<CartItem> {
  final id = 1;
  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ValueKey(id),
      direction: DismissDirection.endToStart,
      background: Container(
        color: Theme.of(context).errorColor,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 30,
        ),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 20),
        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
      ),
      confirmDismiss: (direction) {
        return showDialog(
          context: context,
          builder: (innerContext) => AlertDialog(
            title: Text('Are you sure!'),
            content: Text('Do you want to remove the cart item?'),
            actions: <Widget>[
              FlatButton(
                child: Text('No'),
                onPressed: () {
                  Navigator.of(innerContext).pop(false);
                },
              ),
              FlatButton(
                child: Text("Yes"),
                onPressed: () {
                  Navigator.of(innerContext).pop(true);
                },
              )
            ],
          ),
        );
      },
      onDismissed: (direction) {
        // if(direction == DismissDirection.endToStart) {
        // Provider.of<Categories>(context, listen: false).removeItem(productId);

        // }
      },
      child: Card(
        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        child: Padding(
          padding: EdgeInsets.all(8),
          child: ListTile(
            leading: Container(
              // height: 110,
              width: 80,
              decoration: BoxDecoration(
                color: const Color(0xff7c94b6),
                image: const DecorationImage(
                  image: NetworkImage(
                      'https://freeiconshop.com/wp-content/uploads/edd/box-flat.png'),
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(30),
              ),
            ),
            title: Column(
              children: [
                Row(
                  children: [
                    Text('Book'),
                    Text('Book'),
                  ],
                ),
              ],
            ),
            subtitle: Text("Total: 100"),
            trailing: Text("2 x"),
          ),
        ),
      ),
    );
  }
}
