import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';

class CarouselItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: Carousel(
        images: [
          NetworkImage(
            'https://pustaklab.in/wp-content/uploads/2020/05/banner3-scaled.jpg',
          ),
          NetworkImage(
            'https://pustaklab.in/wp-content/uploads/2020/05/banner-stationery-scaled.jpg',
          ),
          NetworkImage(
            'https://pustaklab.in/wp-content/uploads/2020/05/banner-stationery-2-scaled.jpg',
          ),
        ],
        overlayShadow: true,
        overlayShadowColors: Colors.green[100],
        overlayShadowSize: 0.2,
        showIndicator: false,
        autoplay: true,
      ),
    );
  }
}
