import 'package:flutter/material.dart';

class MainFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Text('Copyright © 2021 | Pustak Lab Developed by Enterpal.'),
    );
  }
}
