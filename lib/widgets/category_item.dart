import 'package:flutter/material.dart';
import 'package:pustak/provider/categories.dart';
import 'package:pustak/provider/dummy_data.dart';

class CategoryList extends StatelessWidget {
  List<Categories> _list = items;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) => Container(
        // height: 80,
        width: 90,
        decoration: BoxDecoration(
          // shape: BoxShape.circle,
          image: DecorationImage(
              image: NetworkImage(
                _list[index].imageUrl,
              ),
              fit: BoxFit.cover),
        ),
      ),
      itemCount: 4,
    );
  }
}
