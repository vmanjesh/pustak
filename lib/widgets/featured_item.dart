import 'package:flutter/material.dart';
import 'package:pustak/provider/categories.dart';
import 'package:pustak/provider/dummy_data.dart';

class FeatureItem extends StatelessWidget {
  List<Categories> _list = items;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GridView.builder(
        physics: NeverScrollableScrollPhysics(),
        itemCount: 4,
        itemBuilder: (BuildContext context, int index) => ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(5.0)),
          child: InkWell(
            onTap: null,
            child: Container(
              decoration: BoxDecoration(
                gradient: RadialGradient(
                    colors: [Colors.grey[500], Colors.grey[700]],
                    center: Alignment(0, 0),
                    radius: 0.6,
                    focal: Alignment(0, 0),
                    focalRadius: 0.1),
              ),
              child: Column(
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Hero(
                        tag: _list[index].id,
                        child: Image.network(_list[index].imageUrl),
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.blue[50],
                    padding: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(_list[index].id),
                        InkWell(
                          child: Icon(Icons.shopping_cart),
                          onTap: () {},
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, crossAxisSpacing: 4, mainAxisSpacing: 10),
      ),
    );
  }
}
