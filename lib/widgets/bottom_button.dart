import 'package:flutter/material.dart';

class BottomCartButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        InkWell(
          onTap: () {},
          child: Container(
            height: 50,
            color: Colors.amber,
            width: MediaQuery.of(context).size.width / 1.8,
            child: Container(
              child: Center(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Text('Total'),
                  SizedBox(
                    width: 2,
                  ),
                  Text('100'),
                ],
              )),
            ),
          ),
        ),
        InkWell(
          onTap: () {},
          child: Container(
            height: 50,
            color: Colors.red,
            width: MediaQuery.of(context).size.width / 2.5,
            child: Center(child: Text('Checkout')),
          ),
        ),
      ],
    );
  }
}
